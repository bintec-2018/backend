package co.com.bintec.bankgo.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import co.com.bintec.bankgo.domain.Transaccion;
import co.com.bintec.bankgo.domain.TransaccionLog;
import co.com.bintec.bankgo.repository.TransaccionLogRepository;
import co.com.bintec.bankgo.repository.TransaccionRepository;

@Service
public class TransaccionService {

	@Autowired
	private TransaccionRepository transaccionRepository;
	
	@Autowired
	private TransaccionLogRepository transaccionLogRepository;

	public ResponseEntity<Transaccion> insertTransaccion(Transaccion request) {
		try {
			if (request != null) {
				Transaccion transaccion = new Transaccion();
				transaccion.setComercio(request.getComercio());
				transaccion.setProducto(request.getProducto());
				transaccion.setValor(request.getValor());
				transaccion.setValorPrestamo(request.getValorPrestamo());
				transaccion.setFecha(new Date());
				transaccionRepository.save(transaccion);
				return new ResponseEntity<>(transaccion, HttpStatus.CREATED);
			} else {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			System.out.println("Error -> " + e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	public ResponseEntity<List<Transaccion>> getTransacciones(String numeroCuenta) {
		try {
			List<Transaccion> listaTransaccion = transaccionRepository.getTransacciones(numeroCuenta);
			return new ResponseEntity<List<Transaccion>>(listaTransaccion, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println("Error -> " + e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	public ResponseEntity<List<TransaccionLog>> getTransaccionesLog() {
		try {
			List<TransaccionLog> transacciones = new ArrayList<>();
//			transaccionLogRepository.findAll().forEach(transacciones::add);
			
//			Iterator<TransaccionLog> iter = (Iterator<TransaccionLog>) transaccionLogRepository.findAll();
			
			List<TransaccionLog> list = transaccionLogRepository.findAllByOrderByIdDesc();
			
			for (TransaccionLog obj : list) {				
				String[] vec = obj.getEstado().split(",");				
				transacciones.add(new TransaccionLog(obj.getId(), obj.getComercio(), obj.getProducto(), obj.getValor(), obj.getFecha(), obj.getValorPrestamo(), vec[0].trim(), vec[1].trim()));
			}
			
			return new ResponseEntity<List<TransaccionLog>>(transacciones, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println("Error -> " + e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
}