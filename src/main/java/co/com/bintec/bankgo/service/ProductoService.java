package co.com.bintec.bankgo.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import co.com.bintec.bankgo.domain.Producto;
import co.com.bintec.bankgo.repository.ProductoRepository;

@Service
public class ProductoService {

	@Autowired
	private ProductoRepository productoRepository;
	
	
 public ResponseEntity<Producto> asociarCuenta(Producto request) {
		try {		
			if (request != null) {
				Producto producto = new Producto();
				producto.setNumeroCuenta(request.getNumeroCuenta());
				producto.setTipo(request.getTipo());
				producto.setSaldo(request.getSaldo());
				producto.setDocumento(request.getDocumento());
				producto.setFecha(new Date());
				productoRepository.save(producto);
				return new ResponseEntity<>(producto, HttpStatus.CREATED);
			} else {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			System.out.println("Error -> " + e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}