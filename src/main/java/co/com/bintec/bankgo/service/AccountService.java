package co.com.bintec.bankgo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import co.com.bintec.bankgo.model.SavingsAccount;
import co.com.bintec.bankgo.model.Token;

@Service
public class AccountService {
	
	Gson gson = new GsonBuilder().create();
	
	@Value("${api.bancolombia.client_id}")
	public String CLIENT_ID;
	
	@Value("${api.bancolombia.client_secret}")
	public String CLIENT_SECRET;
	
	@Value("${api.bancolombia.redirect_uri}")
	public String REDIRECT_URI;
	
	public ResponseEntity<List<SavingsAccount>> getDetail() {
		List<SavingsAccount> cuentas = new ArrayList<>();
		SavingsAccount SavingsAccount = new SavingsAccount("123123123", 1000000);
		try {
			
			
			RestTemplate restTemplate = new RestTemplate();
			
			
			MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
			
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			headers.add("accept", "application/vnd.bancolombia.v1+json");
			headers.add("authorization", "Bearer "+SeguridadService.TOKEN);

			final HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<MultiValueMap<String, String>>(null, headers);
			
//			ResponseEntity<String> response = restTemplate.getForEntity("https://sbapi.bancolombia.com/hackathon/v1/operations/product-specific/accounts/account-details", String.class);
			ResponseEntity<String> response = restTemplate.exchange("https://sbapi.bancolombia.com/hackathon/v1/operations/product-specific/accounts/account-details", HttpMethod.GET, new HttpEntity<Object>(headers), String.class);
			
			if(response.getStatusCode() != null && response.getStatusCode().is2xxSuccessful()) {
				String token = response.getBody();
				System.out.println("RESPONSE [GET DETAIL] -> "+gson.toJson(token));
//				
				
			}
			
			cuentas.add(SavingsAccount);		
			return new ResponseEntity<>(cuentas, HttpStatus.OK);

//		} catch (final HttpClientErrorException httpClientErrorException) {
//			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
//		} catch (HttpServerErrorException httpServerErrorException) {
//			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {			
			cuentas.add(SavingsAccount);			
			System.out.println("Error -> " + e);
			return new ResponseEntity<>(cuentas, HttpStatus.OK);
//			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	
	}
	



}
