package co.com.bintec.bankgo.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import co.com.bintec.bankgo.model.Token;

@Service
public class SeguridadService {
	
	// Url para obtener el code
	//https://sbapi.bancolombia.com/security/oauth-otp/oauth2/authorize?client_id=ef057f5e-ccb7-417b-8803-765a8fcca894&response_type=code&scope=Deposit-account:read:user%20Card-credit:read:user%20Deposit-account:read:user%20Deposit-account:read:user%20Customer-financial:read:user%20Customer-cards:read:user%20Card-credit:read:user%20Card-credit:read:user&redirect_uri=http://localhost:4200/bancos/bancolombia
	
	Gson gson = new GsonBuilder().create();
	
	@Value("${api.bancolombia.client_id}")
	public String CLIENT_ID;
	
	@Value("${api.bancolombia.client_secret}")
	public String CLIENT_SECRET;
	
	@Value("${api.bancolombia.redirect_uri}")
	public String REDIRECT_URI;
	
	public static String TOKEN  = "";
	public static String REFRESH_TOKEN  = "";
	public static String TOKEN_TYPE  = "Bearer";
	
	public ResponseEntity<Token> login(String code) {
		try {
			RestTemplate restTemplate = new RestTemplate();
			
			
			MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
			map.add("grant_type", "authorization_code");
			map.add("code", code);
			map.add("redirect_uri", REDIRECT_URI);
			map.add("client_id", CLIENT_ID);
			map.add("client_secret", CLIENT_SECRET);
			
			System.out.println("REQUEST [GET TOKEN] -> "+gson.toJson(map));
			
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			headers.add("accept", "application/json");

			final HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<MultiValueMap<String, String>>(map, headers);
			
//			ResponseEntity<String> result = restTemplate.postForEntity("https://sbapi.bancolombia.com/security/oauth-otp/oauth2/token", body, String.class);
			ResponseEntity<Token> response = restTemplate.exchange("https://sbapi.bancolombia.com/hackathon/v1/security/oauth-otp/oauth2/token", HttpMethod.POST, entity, Token.class);
			
			if(response.getStatusCode() != null && response.getStatusCode().is2xxSuccessful()) {
				Token token = response.getBody();
				System.out.println("RESPONSE [GET TOKEN] -> "+gson.toJson(token));
//				System.out.println("TOKEN -> "+gson.toJson(token));

				// Guardamos los tokens
				TOKEN = token.getAccessToken();
				REFRESH_TOKEN = token.getRefreshToken();
				
				return new ResponseEntity<>(token, HttpStatus.OK);
			}
			
			return new ResponseEntity<>(HttpStatus.OK);

		} catch (final HttpClientErrorException httpClientErrorException) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} catch (HttpServerErrorException httpServerErrorException) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			System.out.println("Error -> " + e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	
	}
	
	public void refreshToken() {
		try {
			
			if(!REFRESH_TOKEN.isEmpty()) {
			RestTemplate restTemplate = new RestTemplate();			
			
			MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
			map.add("grant_type", "refresh_token");
			map.add("client_id", CLIENT_ID);
			map.add("client_secret", CLIENT_SECRET);
			map.add("refresh_token", REFRESH_TOKEN);
			
			System.out.println("REQUEST [REFRESH TOKEN] -> "+gson.toJson(map));
			
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			headers.add("accept", "application/json");

			final HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<MultiValueMap<String, String>>(map, headers);
			
//			ResponseEntity<String> result = restTemplate.postForEntity("https://sbapi.bancolombia.com/security/oauth-otp/oauth2/token", body, String.class);
			ResponseEntity<Token> response = restTemplate.exchange("https://sbapi.bancolombia.com/hackathon/v1/security/oauth-otp/oauth2/token", HttpMethod.POST, entity, Token.class);
			
			if(response.getStatusCode() != null && response.getStatusCode().is2xxSuccessful()) {
				Token token = response.getBody();
				System.out.println("RESPONSE [REFRESH TOKEN] -> "+gson.toJson(token));
				TOKEN = token.getAccessToken();
				REFRESH_TOKEN = token.getRefreshToken();
//				
			}
			}
		} catch (Exception e) {
			System.out.println("Error -> " + e);
		}	
	}



}
