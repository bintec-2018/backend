package co.com.bintec.bankgo.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Token {

	@JsonProperty("token_type")
	private String TokenType;
	
	@JsonProperty("access_token")
	private String AccessToken;
	
	@JsonProperty("expires_in")
	private int ExpiresIn;
	
	@JsonProperty("consented_on")
	private long ConsentedOn;
	
	@JsonProperty("scope")
	private String Scope;
	
	@JsonProperty("refresh_token")
	private String RefreshToken;
	
	@JsonProperty("refresh_token_expires_in")
	private long RefreshTokenExpiresIn;
	
	public Token() {
		super();
	}
	
	public String getTokenType() {
		return TokenType;
	}
	public void setTokenType(String tokenType) {
		TokenType = tokenType;
	}
	public String getAccessToken() {
		return AccessToken;
	}
	public void setAccessToken(String accessToken) {
		AccessToken = accessToken;
	}
	public int getExpiresIn() {
		return ExpiresIn;
	}
	public void setExpiresIn(int expiresIn) {
		ExpiresIn = expiresIn;
	}
	public long getConsentedOn() {
		return ConsentedOn;
	}
	public void setConsentedOn(long consentedOn) {
		ConsentedOn = consentedOn;
	}
	public String getScope() {
		return Scope;
	}
	public void setScope(String scope) {
		Scope = scope;
	}
	public String getRefreshToken() {
		return RefreshToken;
	}
	public void setRefreshToken(String refreshToken) {
		RefreshToken = refreshToken;
	}
	public long getRefreshTokenExpiresIn() {
		return RefreshTokenExpiresIn;
	}
	public void setRefreshTokenExpiresIn(long refreshTokenExpiresIn) {
		RefreshTokenExpiresIn = refreshTokenExpiresIn;
	}
	
	
	
}
