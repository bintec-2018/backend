package co.com.bintec.bankgo.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SavingsAccount {
	
	@JsonProperty("account_number")
	private String accountNumber;
	
	@JsonProperty("savingsAccount_availableBalance")
	private double savingsAccountAvailableBalance;

	public SavingsAccount() {
		super();
	}

	public SavingsAccount(String accountNumber, double savingsAccountAvailableBalance) {
		super();
		this.accountNumber = accountNumber;
		this.savingsAccountAvailableBalance = savingsAccountAvailableBalance;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public double getSavingsAccountAvailableBalance() {
		return savingsAccountAvailableBalance;
	}

	public void setSavingsAccountAvailableBalance(double savingsAccountAvailableBalance) {
		this.savingsAccountAvailableBalance = savingsAccountAvailableBalance;
	}
	
	

}
