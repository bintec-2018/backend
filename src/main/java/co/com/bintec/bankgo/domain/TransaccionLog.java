package co.com.bintec.bankgo.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "transaccion_log")
public class TransaccionLog implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne
	@JoinColumn(name="id_comercio")
	private Comercio comercio;
	
	@ManyToOne
	@JoinColumn(name="id_producto")
	private Producto producto;
	
	@NotNull
	private double valor;
	
	private Date fecha;
		
	@Column(name="valor_prestamo")
	private double valorPrestamo;
	
	private String estado;
	
	private String descripcion;
	
	public TransaccionLog() {
		super();
	}

	public TransaccionLog(int id, Comercio comercio, Producto producto, @NotNull double valor, Date fecha,
			double valorPrestamo, String estado, String descripcion) {
		super();
		this.id = id;
		this.comercio = comercio;
		this.producto = producto;
		this.valor = valor;
		this.fecha = fecha;
		this.valorPrestamo = valorPrestamo;
		this.estado = estado;
		this.descripcion = descripcion;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Comercio getComercio() {
		return comercio;
	}

	public void setComercio(Comercio comercio) {
		this.comercio = comercio;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public double getValorPrestamo() {
		return valorPrestamo;
	}

	public void setValorPrestamo(double valorPrestamo) {
		this.valorPrestamo = valorPrestamo;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
	
}
