package co.com.bintec.bankgo.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;

@Entity
@NamedQuery(name="Comercio.findAll", query="SELECT c FROM Comercio c")
public class Comercio implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	private int id;
	
	@NotNull
	private String nombre;
	
	public Comercio() {
		super();
	}
	
	public Comercio(int id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
	

}
