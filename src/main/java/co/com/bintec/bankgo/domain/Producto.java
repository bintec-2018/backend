package co.com.bintec.bankgo.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;

@Entity
@NamedQuery(name="Producto.findAll", query="SELECT p FROM Producto p")
public class Producto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="numero_cuenta")
	private String numeroCuenta;
	
	@NotNull
	private String tipo;
	
	@NotNull
	private double saldo; 
	
	@NotNull
	private String documento;
	
	private Date fecha;
	
	public Producto() {
		super();
	}

	public Producto(String numeroCuenta, @NotNull String tipo, @NotNull double saldo, @NotNull String documento,
			Date fecha) {
		super();
		this.numeroCuenta = numeroCuenta;
		this.tipo = tipo;
		this.saldo = saldo;
		this.documento = documento;
		this.fecha = fecha;
	}

	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	
}
