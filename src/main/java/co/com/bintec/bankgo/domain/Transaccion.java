package co.com.bintec.bankgo.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;

@Entity
@NamedQuery(name="Transaccion.findAll", query="SELECT t FROM Transaccion t")
public class Transaccion implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne
	@JoinColumn(name="id_comercio")
	private Comercio comercio;
	
	@ManyToOne
	@JoinColumn(name="id_producto")
	private Producto producto;
	
	@NotNull
	private double valor;
	
	private Date fecha;
		
	@Column(name="valor_prestamo")
	private double valorPrestamo;
	
	public Transaccion() {
		super();
	}
	
	public Transaccion(int id, String numeroCuenta, Comercio comercio, Producto producto, double valor,
			Date fecha, double valorPrestamo) {
		super();
		this.id = id;
		this.comercio = comercio;
		this.producto = producto;
		this.valor = valor;
		this.fecha = fecha;
		this.valorPrestamo = valorPrestamo;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Comercio getComercio() {
		return comercio;
	}
	public void setComercio(Comercio comercio) {
		this.comercio = comercio;
	}
	public Producto getProducto() {
		return producto;
	}
	public void setProducto(Producto producto) {
		this.producto = producto;
	}
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public double getValorPrestamo() {
		return valorPrestamo;
	}

	public void setValorPrestamo(double valorPrestamo) {
		this.valorPrestamo = valorPrestamo;
	}
	
}
