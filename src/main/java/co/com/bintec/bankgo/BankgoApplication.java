package co.com.bintec.bankgo;

import java.util.Date;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BankgoApplication {

	public static void main(String[] args) {
		System.out.println("SUBIO "+new Date());
		SpringApplication.run(BankgoApplication.class, args);
	}
}
