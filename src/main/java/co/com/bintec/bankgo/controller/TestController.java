package co.com.bintec.bankgo.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/test")
@CrossOrigin(origins = "*")
public class TestController {

	@RequestMapping( method = RequestMethod.GET, produces = "application/json")
	public  ResponseEntity<String> holaMundo(HttpServletRequest  request){
		
		System.out.println("ENTRO A TEST");
		return new ResponseEntity<>("Hola", HttpStatus.OK);
	}

}
