package co.com.bintec.bankgo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.bintec.bankgo.model.SavingsAccount;
import co.com.bintec.bankgo.service.AccountService;

@RestController 
@RequestMapping("/accounts")
@CrossOrigin(origins = "*")
public class AccountController {
	
	@Autowired
	private AccountService accountService;	
	
	@RequestMapping(value = "/account-details", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<List<SavingsAccount>> getDetail() {
		try {
//			
			return accountService.getDetail();
		} catch (Exception e) {
			System.out.println("Error -> " + e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
