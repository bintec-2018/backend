package co.com.bintec.bankgo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.bintec.bankgo.model.Token;
import co.com.bintec.bankgo.service.SeguridadService;

@RestController 
@RequestMapping("/seguridad")
@CrossOrigin(origins = "*")
public class SeguridadController {
	
	@Autowired
	private SeguridadService seguridadService;	
	
	@RequestMapping(value = "/login/{code}", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Token> login(@PathVariable String code) {
		try {
			System.out.println("CODE:" + code);
//			
			return seguridadService.login(code);
		} catch (Exception e) {
			System.out.println("Error -> " + e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
