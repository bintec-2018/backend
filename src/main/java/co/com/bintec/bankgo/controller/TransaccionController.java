package co.com.bintec.bankgo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.com.bintec.bankgo.domain.Transaccion;
import co.com.bintec.bankgo.domain.TransaccionLog;
import co.com.bintec.bankgo.service.TransaccionService;

@RestController 
@RequestMapping("/transacciones")
@CrossOrigin(origins = "*")
public class TransaccionController {
	
	@Autowired
	TransaccionService transaccionService;
	
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Transaccion> insertarTransaccion(@RequestBody Transaccion request) {
		try {			
			return transaccionService.insertTransaccion(request);
		} catch (Exception e) {
			System.out.println("Error -> " + e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<List<Transaccion>> getTransacciones(@RequestParam(name="numeroCuenta") String numeroCuenta) {
		try {	
			return transaccionService.getTransacciones(numeroCuenta);
		} catch (Exception e) {
			System.out.println("Error -> " + e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/logs", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<List<TransaccionLog>> getTransaccionesLog() {
		try {	
			return transaccionService.getTransaccionesLog();
		} catch (Exception e) {
			System.out.println("Error -> " + e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
