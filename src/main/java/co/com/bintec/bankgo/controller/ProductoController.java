package co.com.bintec.bankgo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.bintec.bankgo.domain.Producto;
import co.com.bintec.bankgo.service.ProductoService;

@RestController 
@RequestMapping("/producto")
@CrossOrigin(origins = "*")
public class ProductoController {
	
	@Autowired
	private ProductoService productoService;
	
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Producto> asociarCuenta(@RequestBody Producto request) {
		try {
			return productoService.asociarCuenta(request);
		} catch (Exception e) {
			System.out.println("Error -> " + e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
