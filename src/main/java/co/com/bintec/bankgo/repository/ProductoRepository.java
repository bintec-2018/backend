package co.com.bintec.bankgo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import co.com.bintec.bankgo.domain.Producto;

@Repository
public interface ProductoRepository extends CrudRepository<Producto, Integer>{
	
}
