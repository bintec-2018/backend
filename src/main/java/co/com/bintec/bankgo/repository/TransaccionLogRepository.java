package co.com.bintec.bankgo.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import co.com.bintec.bankgo.domain.TransaccionLog;

@Repository
public interface TransaccionLogRepository extends CrudRepository<TransaccionLog, Integer>{
	
	List<TransaccionLog> findAllByOrderByIdDesc();
	
}
