package co.com.bintec.bankgo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import co.com.bintec.bankgo.domain.Transaccion;

@Repository
public interface TransaccionRepository extends CrudRepository<Transaccion, Integer>{
	@Query(value = "select  *  from transaccion  where id_producto = ?1 order by id desc", nativeQuery = true)	
	List<Transaccion> getTransacciones(String numeroCuenta);
}
