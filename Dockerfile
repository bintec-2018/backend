FROM openjdk:8-jdk-alpine
COPY build/libs/backend-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
#RUN ./gradlew --project-cache-dir=$WERCKER_CACHE_DIR/.gradle  --full-stacktrace build
## && java -jar build/libs/backend-0.0.1-SNAPSHOT.jar

EXPOSE 8080
