-- DATABASE
drop database hackaton_bintec;
create database hackaton_bintec;

use hackaton_bintec;


CREATE TABLE comercio (
    id            INT NOT NULL,
    nombre   VARCHAR(100),
    PRIMARY KEY (id)
);

CREATE TABLE producto (
    numero_cuenta       VARCHAR(100) NOT NULL,
    tipo   VARCHAR(100) NOT NULL,
    saldo 				double DEFAULT 0,
    documento   VARCHAR(100) NOT NULL,
    fecha TIMESTAMP default CURRENT_TIMESTAMP,
    PRIMARY KEY (numero_cuenta)
);

CREATE TABLE transaccion (
    id                  INT NOT NULL AUTO_INCREMENT,
    id_producto           	VARCHAR(100) NOT NULL,
    id_comercio    	INT NOT NULL,
    valor 				double NOT NULL,
    fecha			TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    valor_prestamo 				double DEFAULT 0,
    PRIMARY KEY (id)
);

CREATE TABLE transaccion_log (
    id                  INT NOT NULL AUTO_INCREMENT,
    id_producto           	VARCHAR(100) NOT NULL,
    id_comercio    	INT NOT NULL,
    valor 				double NOT NULL,
    fecha			TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    valor_prestamo 				double DEFAULT 0,
    estado           	VARCHAR(100) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE repo_foto (
    id            INT NOT NULL AUTO_INCREMENT,
    valor   longtext,
    img_base64 longtext,
    documento VARCHAR(100) NOT NULL,
    fecha TIMESTAMP default CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);

ALTER TABLE transaccion ADD CONSTRAINT transaccion_comercio_fk FOREIGN KEY ( id_comercio ) REFERENCES comercio ( id );
ALTER TABLE transaccion ADD CONSTRAINT transaccion_producto_fk FOREIGN KEY ( id_producto ) REFERENCES producto ( numero_cuenta );

ALTER TABLE transaccion_log ADD CONSTRAINT transaccion_log_comercio_fk FOREIGN KEY ( id_comercio ) REFERENCES comercio ( id );
ALTER TABLE transaccion_log ADD CONSTRAINT transaccion_log_producto_fk FOREIGN KEY ( id_producto ) REFERENCES producto ( numero_cuenta );